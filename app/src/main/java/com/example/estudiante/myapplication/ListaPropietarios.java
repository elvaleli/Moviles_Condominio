package com.example.estudiante.myapplication;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.estudiante.myapplication.Servicios.Util;
import com.example.estudiante.myapplication.Servicios.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ListaPropietarios extends AppCompatActivity {
    private String id;
    private ListView lista;
    private Context ctx;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_propietarios);
        ctx=this;
        lista=(ListView) findViewById(R.id.lista_area);
        this.cargarLista();
    }

    private void cargarLista(){
        String url= Util.getUrl()+"listar_propietarios";
        Log.d("myTag", "This is my message");
        JsonObjectRequest objeto=new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    System.out.print(response);
                    List<String> items = new ArrayList<>();
                    JSONArray array= response.getJSONArray("array");
                    for(int i=0;i<array.length();i++)
                    {
                        JSONObject object= array.getJSONObject(i);
                        System.err.print(object.getString("nombre"));
                        items.add(object.getString("nombre"));
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(ctx, android.R.layout.simple_list_item_1, items);
                    lista.setAdapter(adapter);


                    JSONObject nested= response.getJSONObject("nested");
                    Log.d("TAG","flag value "+nested.getBoolean("flag"));

                    //ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, values);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        VolleySingleton.getInstance(this).addToRequestQueue(objeto);
    }
}
