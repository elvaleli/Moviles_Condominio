package com.example.estudiante.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Quejas extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quejas);
    }


    public void crearQuejas(View vista){
        Intent intent= new Intent(this, crearQueja.class);
        startActivity(intent);

    }
}
