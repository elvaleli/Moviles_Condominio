package com.example.estudiante.myapplication;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.estudiante.myapplication.Servicios.Util;
import com.example.estudiante.myapplication.Servicios.VolleySingleton;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener  {

    TextInputEditText textuser,txtpassword;
    Button btnlogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textuser=(TextInputEditText) findViewById(R.id.textusername);
        txtpassword = (TextInputEditText) findViewById(R.id.textpassword);
        btnlogin = (Button) findViewById(R.id.btnlogin);
        btnlogin.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        /*Usando un AsyncTask para el logueo.*/
        if(textuser.getText().toString().trim().equals("")|| txtpassword.getText().toString().trim().equals("")){
            Toast.makeText(getApplicationContext(), "Por favor complete los campos", Toast.LENGTH_SHORT).show();
        }else{

            Log.d("myTag", "This is my message");
            String url=Util.getUrl()+"iniciar_sesion";
            JsonObjectRequest objeto=new JsonObjectRequest(Request.Method.GET, url+"?usuario="+ textuser.getText().toString()+"&&contrasena="+txtpassword.getText().toString(), null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Intent intent = new Intent(getApplicationContext(), Menu.class);
                        intent.putExtra("id", response.getString("id"));
                        intent.putExtra("nombre", response.getString("nombre"));
                        intent.putExtra("apellido",response.getString("apellido"));
                        intent.putExtra("correo",response.getString("correo"));
                        startActivity(intent);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    //Toast.makeText(getApplicationContext(), "ha iniciado session", Toast.LENGTH_SHORT).show();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("usuario", textuser.getText().toString());
                    params.put("contrasena", txtpassword.getText().toString());
                    return params;
                }
            };

            VolleySingleton.getInstance(this).addToRequestQueue(objeto);

        }

    }

    public void crearUsuario(View vista){
        Intent intent= new Intent(this, CrearRegistro.class);
        startActivity(intent);

    }
}
