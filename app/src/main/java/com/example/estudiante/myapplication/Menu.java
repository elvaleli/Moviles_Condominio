package com.example.estudiante.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class Menu extends AppCompatActivity {
    private String id;
    private String nombre;
    private String apellido;
    private String correo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        id=getIntent().getStringExtra("id");
        nombre=getIntent().getStringExtra("nombre");
        apellido=getIntent().getStringExtra("apellido");
        correo=getIntent().getStringExtra("correo");
    }

    public void irQuejas(View vista){
        Intent intent= new Intent(this, Quejas.class);
        intent.putExtra("id", id);
        startActivity(intent);

    }

    public void irPrestamo(View vista){
        Intent intent= new Intent(this, PrestamoArea.class);
        intent.putExtra("id", id);
        startActivity(intent);

    }

    public void irParqueadero(View vista){
        Intent intent= new Intent(this, Parqueadero.class);
        intent.putExtra("id", id);
        startActivity(intent);

    }

    public void irPago(View vista){
        Intent intent= new Intent(this, PagoCondominio.class);
        intent.putExtra("id", id);
        startActivity(intent);

    }

    public void irPropietarios(View vista){
        Intent intent= new Intent(this, ListaPropietarios.class);
        intent.putExtra("id", id);
        startActivity(intent);

    }

    public void irMenuArea(View vista){
        Intent intent= new Intent(this, MenuArea.class);
        intent.putExtra("id", id);
        startActivity(intent);

    }
}
